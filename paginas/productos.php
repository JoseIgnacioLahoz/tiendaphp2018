<h2>
	Productos
	<?php 
	if($_SESSION['conectado']){
	?>
	-
	<small>
		<a href="index.php?p=productos.php&accion=insertar">Insertar producto</a>
	</small>
	<?php } ?>

	<small>
		<form action="index.php" method="get" class="form-inline text-right">
			<input type="text" class="form-control" name="palabra" value="" placeholder="Buscar...">
			<input type="submit" name="buscar" value="Buscar" class="btn btn-info">
			<input type="hidden" name="p" value="productos.php">
			<input type="hidden" name="accion" value="buscador">
		</form>
	</small>
</h2>
<?php 
// Este archivo va a recibir una acción sino es así, listará los productos
if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}else{
	$accion='listado';
}

//Recogemos en que pagina estamos para mostrar los resultados que le corresponda
if(isset($_GET['numeroDePagina'])){
	$numeroDePagina=$_GET['numeroDePagina'];
}else{
	$numeroDePagina=0;
}

//Dependiendo de $accion la web hace una cosa u otra
switch ($accion) {
	/////////////////////////////////////////////////////////////
	/////////////// LISTADO DE PRODUCTOS  ///////////////////////
	/////////////////////////////////////////////////////////////
	case 'listado':
		if($_SESSION['conectado']){
		?>
		<!-- <h4>
			<a href="index.php?p=productos.php&accion=insertar">
				Insertar producto
			</a>
		</h4> -->
		<?php 
		}
		
		//Establecer la consulta a la base de datos en SQL
		$sql="SELECT * FROM productos";
		//Ejecutar la pregunta o consulta
		$consulta=$conexion->query($sql);

		//Para hacer un listado con paginación. Primero averiguamos el numero total de resgistros
		$numeroTotalDeRegistros=$consulta->num_rows;
		//Establecer el numero de registros por pagina
		$numeroDeRegistrosPorPagina=4; //Lo ponemos a mano
		

		//Consulta según el numero de paginas obtenidas
		$inicioLimite=$numeroDePagina*$numeroDeRegistrosPorPagina;
		$sql="SELECT * FROM productos LIMIT $inicioLimite,$numeroDeRegistrosPorPagina";

		//Vuelvo a ejecutar la consulta pero con su LIMIT ya paginado
		$consulta=$conexion->query($sql);

		//Procesamos los resultados de la pregunta con un bucle
		while($registro=$consulta->fetch_array()){
			?>
			<article>
				<header>
					<h4>
						<a href="index.php?p=productos.php&accion=ver&id=<?php echo $registro['idProd'];?>&numeroDePagina=<?php echo $numeroDePagina; ?>">
						<?php
						//Inicio de Mostrar la primera imagen del producto
						//Accedo a la bbdd para mostrar las imagenes que elijo de la carpeta imagenes
			    		$sqli="SELECT * FROM imagenes WHERE idProd={$registro['idProd']}";
			    		$consultai=$conexion->query($sqli);
			    		$registroi=$consultai->fetch_array();
			    			?>
			    			<img src="imagenes/<?php echo $registroi['archivoImg']; ?>" width="50">
			    		<!-- Fin de Mostrar la primera imagen del producto -->

							<strong><?php echo $registro['nombreProd']; ?></strong>
						</a>

						<small>(<?php echo dimeFechaCorta($registro['fechaAlta']); ?>)</small>

						<?php 
						if($_SESSION['conectado']){
						?>
						-
						<a href="index.php?p=productos.php&accion=borrar&id=<?php echo $registro['idProd'];?>&numeroDePagina=<?php echo $numeroDePagina; ?>">
							<span class="glyphicon glyphicon-trash" style="color: red;"></span>
						</a>
						-
						<a href="index.php?p=productos.php&accion=modificar&id=<?php echo $registro['idProd'];?>&numeroDePagina=<?php echo $numeroDePagina; ?>">
							<span class="glyphicon glyphicon-pencil" style="color: green;"></span>
						</a>
						<?php } ?>
					</h4>
					<!-- <small>
						<?php echo $registro['nombreCat']; ?>
					</small> -->
				</header>
			</article>
			<hr>
		<?php
		}

		?>	
		<div class="text-center">
			<ul class="pagination">
			<?php
				//Inicio if condicional para no seguir recorriendo paginas hacia atras cuando se llega a la pagina 0
			 if($numeroDePagina==0){ ?>
				<li class="disabled"><a href="#">&laquo;</a></li>
			<?php }else{?>
				<li>
			  		<a href="index.php?p=productos.php&accion=listado&numeroDePagina=<?php echo ($numeroDePagina-1); ?>">&laquo;</a>
			  	</li>
			<?php
				} //Fin del if para no bajar de la pagina 0

				//Calculo el numero total de paginas
				$numeroTotalDePaginas=ceil($numeroTotalDeRegistros/$numeroDeRegistrosPorPagina);
					//Bucle para los numeros de las paginas que muestro
					for($numPagina=0;$numPagina<$numeroTotalDePaginas;$numPagina++){
						//Condicional para marcar la pagina activa y que la marque
						if($numeroDePagina==$numPagina){
							$activa='active';
						}else{
							$activa='';
						}
				?>

				<li class="<?php echo $activa; ?>">
					<a href="index.php?p=productos.php&accion=listado&numeroDePagina=<?php echo $numPagina; ?>"><?php echo ($numPagina+1); ?>		
					</a>
				</li>

				<?php 
				} //Fin del bulce for
				?>
			
			<?php
				//Inicio if condicional para no sobrepasar recorriendo paginas hacia adeanate cuando se llega a la úlltima
			 if($numeroDePagina==($numeroTotalDePaginas-1)){ ?>
				<li class="disabled"><a href="#">&raquo;</a></li>
			<?php }else{?>
				<li>
			  		<a href="index.php?p=productos.php&accion=listado&numeroDePagina=<?php echo ($numeroDePagina+1); ?>">&raquo;</a>
			  	</li>
			<?php
				} //Fin del if para no pasar de la última pagina 
			?>
			</ul>
		</div>
		<?php
		break;
	
	//////////////////////////////////////////////////////////////
	////////////////// INSERTAR PRODUCTO   ////////////////
	//////////////////////////////////////////////////////////////
	case 'insertar':
		if($_SESSION['conectado']){
		?>
		<h3>
			Insertar un producto
			-
			<small>
				<a href="index.php?p=productos.php">Volver / Cancelar</a>
			</small>
		</h3>
		<hr>
		
		<?php 
		 
		if (isset($_POST['enviar'])){
			//Inserto el producto
			 
			//Recojo los datos que quiero insertar
			$nombreProd=$_POST['nombreProd'];
			$descripcionProd=$_POST['descripcionProd'];
			$precioProd=$_POST['precioProd'];
			$unidadesProd=$_POST['unidadesProd'];
			$fechaAlta=$_POST['fechaAlta'];
			if(isset($_POST['activado'])){
				$activado=1; //Sera cuando esta checked
			}else{
				$activado=0; //Sera cuando NO ESTA checked
			}

			//Establezco la consulta
			$sql="INSERT INTO productos(nombreProd, descripcionProd, precioProd, unidadesProd, fechaAlta, activado)VALUES('$nombreProd', '$descripcionProd', $precioProd, $unidadesProd, '$fechaAlta', $activado)";

			//Ejecuto la consulta y/o Muestro el mensaje
			if($consulta=$conexion->query($sql)){
			/////////////////// INSERCCIÓN DE IMAGENES //////////////////////////////////////////
			
			//Recoger el último id de producto insertado
				$idProd=$conexion->insert_id;

			//Recorrer el vector de $_FILES['imagenes']con las imagenes
				for($i=0;$i<count($_FILES['imagenes']['name']);$i++){
					if(is_uploaded_file($_FILES['imagenes']['tmp_name'][$i])){
						//Ubicamos la imagen en su sitio
						$nombreImagen=(time()+rand(1,10000)).'_'.$_FILES['imagenes']['name'][$i];
						move_uploaded_file($_FILES['imagenes']['tmp_name'][$i], 'imagenes/'.$nombreImagen);
						//Insertamos la imagen en la bbdd
						$sqli="INSERT INTO imagenes(archivoImg, idProd)VALUES('$nombreImagen', $idProd)";
						$consultai=$conexion->query($sqli);
					}
					
				}
			/////////////// FIN INSERCCIÓN DE IMAGENES //////////////////////////////////////////

				header('Refresh: 2; url=index.php?p=productos.php&numeroDePagina='.$numeroDePagina);
				?>
				<div class="alert alert-success">
					<strong>TODO OK!!</strong>
					Insercion realizada con éxito
					<img src="imagenes/cargando.gif" width="50">
				</div>
				<?php	
			}else{
				?>
				<div class="alert alert-danger">
					<strong>ERROR!!</strong>
					No se ha podido realizar
				</div>
				<?php
			}
		}else{
			//Muestro el formulario de insercción
		?>
		<form action="index.php?p=productos.php&accion=insertar&numeroDePagina=<?php echo $numeroDePagina;?>" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label for="nombreProd">Nombre del producto:</label>
				<input type="text" class="form-control" name="nombreProd" id="nombreProd">
			</div> 

			<div class="form-group">
				<label for="descripcionProd">Descripción del producto:</label>
				<textarea class="form-control"  name="descripcionProd" id="descripcionProd"></textarea>		
			</div>

			<div class="form-group">
				<label for="precioProd">Precio del producto:</label>
				<input type="text" class="form-control"  name="precioProd" id="precioProd">		
			</div>

			<div class="form-group">
				<label for="unidadesProd">Existencias del producto:</label>
				<input type="text" class="form-control"  name="unidadesProd" id="unidadesProd">		
			</div>

			<div class="form-group">
				<label for="fechaAlta">Fecha de alta del producto:</label>
				<input type="date" class="form-control"  name="fechaAlta" id="fechaAlta" value="<?php echo date('Y-m-d'); ?>" max="<?php echo date('Y-m-d'); ?>">		
			</div>

			<div class="form-group">
				<label for="activado">Producto activado:</label>
				<input type="checkbox"  name="activado" id="activado" checked>
			</div>

			<div class="form-group" id="imagenes">
				<label for="imagenes">
					Imagenes:
					<a href="#">Agregar otra</a>
				</label>
				<input type="file" name="imagenes[]">
			</div>

			<button type="sumbit" name="enviar" class="btn btn-primary btn-lg btn-block">
				Enviar
			</button>
		</form>

		<?php 
		}
		}else{
			?>
				<div class="alert alert-danger">No tienes permiso para realizar esta acción</div>;
			<?php
		}// Fin del if ($_SESSION['conectado'])
		break;

	//////////////////////////////////////////////////////////////
	////////////////// VER DETALLE DEL PRODUCTO   ////////////////
	//////////////////////////////////////////////////////////////
	case 'ver':
		?>	
		<h3>
			Detalle del producto
		</h3>

	<?php 
	// Recojo el idProducto que quiero mostrar
	$id=$_GET['id'];

	//Establezco una consulta segun su id de producto
	$sql="SELECT * FROM productos WHERE idProd=$id";

	// Ejecuto la consulta
	$consulta=$conexion->query($sql);

	// Extraigo los datos de dicha consulta
	$registro=$consulta->fetch_array();
	?>

	<article>
		<header>
			<h4>
				<strong><?php echo $registro['idProd']; ?></strong>
				<a href="index.php?p=productos.php&numeroDePagina=<?php echo $numeroDePagina; ?>"> - Volver</a>
			</h4>
		</header>
		<section>
			<div class="panel panel-success">
			  <div class="panel-heading">
			  	<!-- Muestro del nombre del producto -->
			    <h3 class="panel-title">Producto seleccionado: <strong><?php echo $registro['nombreProd']; ?></strong></h3>
			  </div>
			  <div class="panel-body">
			  	
			    <?php 
			    //Accedo a la bbdd para mostrar las imagenes que elijo de la carpeta imagenes
			    $sqli="SELECT * FROM imagenes WHERE idProd=$id";
			    $consultai=$conexion->query($sqli);
			    while($registroi=$consultai->fetch_array()){
			    	?>
			    	<img src="imagenes/<?php echo $registroi['archivoImg'] ?>" width="235">
			    
			    	<?php
			    }

			     ?>

			  </div>
			</div>

			<div class="panel panel-info">
			  <div class="panel-heading">
			    <h3 class="panel-title">Descripción:</h3>
			  </div>
			  <div class="panel-body">
			    <?php echo $registro['descripcionProd']; ?>
			  </div>
			</div>

			<div class="panel panel-info">
			  <div class="panel-heading">
			    <h3 class="panel-title">Precio:</h3>
			  </div>
			  <div class="panel-body">
			    <?php echo $registro['precioProd']; ?> €
			  </div>
			</div>

			<div class="panel panel-info">
			  <div class="panel-heading">
			    <h3 class="panel-title">Stock disponible:</h3>
			  </div>
			  <div class="panel-body">
			    <?php echo $registro['unidadesProd']; ?> unidades
			  </div>
			</div>
			
			<div class="panel panel-info">
			  <div class="panel-heading">
			    <h3 class="panel-title">Disponible desde:</h3>
			  </div>
			  <div class="panel-body">
			    <?php
			    	echo dimeFecha($registro['fechaAlta']);
			    ?> 
			  </div>
			</div>

		</section>
	</article>
	<?php
		break;

	///////////////////////////////////////////////////////////////////
	////////////////  BORRAR UN PRODUCTO DEL LISTADO  ////////////////
	//////////////////////////////////////////////////////////////////	

	case 'borrar':
		if($_SESSION['conectado']){
				//Cojo el id del producto a borrar
		$id=$_GET['id'];

		//Establezco la consulta
		$sql="DELETE FROM productos WHERE idProd=$id";

		//Ejecuto la consulta y/o Muestro mensaje
		if($consulta=$conexion->query($sql)){
			//Si se borra el producto se borran sus imagenes
			//Primero borro las imagenes fisicas del directorio
			$sqli="DELETE FROM imagenes WHERE idProd=$id";
			$consultai=$conexion->query($sqli);
			while ($registroi=$consultai->fetch_array()) {
				unlink('imagenes/'.$registroi['archivoImg']);
			}
			//Una vez borradas del directorio,las borro de la bbdd
			$sqli="DELETE FROM imagenes WHERE idProd=$id";
			$consultai=$conexion->query($sqli);

			header('Refresh: 2; url=index.php?p=productos.php');
			?>
			<div class="alert alert-success">
				<strong>TODO OK!!</strong>
				Borrado realizado con exito
				<img src="imagenes/cargando.gif" width="50">
			</div>
			<?php		
		}else{
			?>
			<div class="alert alert-danger">
				<strong>ERROR!!</strong>
				No se ha podido realizar
			</div>
			<?php 
			 }
		}else{
			?>
				<div class="alert alert-danger">No tienes permiso para realizar esta acción</div>;
			<?php
		} // Fin del if ($_SESSION['conectado'])
		break;
	//////////////////////////////////////////////////////////////////////
	///////////////    MODIFICAR UN PRODUCTO   //////////////////////////
	//////////////////////////////////////////////////////////////////////
	case 'modificar':
		if($_SESSION['conectado']){
		?>
		<h3>
			Modificar un producto
			-
			<small>
				<a href="index.php?p=productos.php&numeroDePagina=<?php echo $numeroDePagina; ?>">Volver / Cancelar</a>
			</small>
		</h3>
		<hr>
		<?php
		if (isset($_POST['enviar'])){
			//Modifico el producto
			//Recojo los datos que quiero modificar
			$nombreProd=$_POST['nombreProd'];
			$descripcionProd=$_POST['descripcionProd'];
			$precioProd=$_POST['precioProd'];
			$unidadesProd=$_POST['unidadesProd'];
			$fechaAlta=$_POST['fechaAlta'];

			$id=$_POST['idProd'];
			if(isset($_POST['activado'])){
				$activado=1; //Sera cuando esta checked
			}else{
				$activado=0; //Sera cuando NO ESTA checked
			}
			
				//Establezco la consulta
			$sql="UPDATE productos SET nombreProd='$nombreProd', descripcionProd='$descripcionProd', precioProd=$precioProd, unidadesProd=$unidadesProd, fechaAlta='$fechaAlta', activado=$activado WHERE idProd=$id";

			//Ejecuto la consulta y/o Muestro el mensaje
			if($consulta=$conexion->query($sql)){
			//Aquí empezamos la agregación de las imagenes que vienen de una modificación
			$idProd=$id;
				
			//Recorrer el vector de $_FILES['imagenes']con las imagenes
				for($i=0;$i<count($_FILES['imagenes']['name']);$i++){
					if(is_uploaded_file($_FILES['imagenes']['tmp_name'][$i])){
						//Ubicamos la imagen en su sitio
						$nombreImagen=(time()+rand(1,10000)).'_'.$_FILES['imagenes']['name'][$i];
						move_uploaded_file($_FILES['imagenes']['tmp_name'][$i], 'imagenes/'.$nombreImagen);
						//Insertamos la imagen en la bbdd
						$sqli="INSERT INTO imagenes(archivoImg, idProd)VALUES('$nombreImagen', $idProd)";
						$consultai=$conexion->query($sqli);
					}	
				}

				//Fin de la agregación de las imagenes que vienen de una modificación
				header('Refresh: 2; url=index.php?p=productos.php&numeroDePagina='.$numeroDePagina);
				?>
				<div class="alert alert-success">
					<strong>TODO OK!!</strong>
					Modificación realizada con éxito
					<img src="imagenes/cargando.gif" width="50">
				</div>
				<?php	
			}else{
				?>
				<div class="alert alert-danger">
					<strong>ERROR!!</strong>
					No se ha podido realizar
				</div>
				<?php
			}
		}else{
			//Muestro el formulario de modificación
			//Necesito el id del producto  a modificar para poder actuar sobre él
			$id=$_GET['id'];
			$sql="SELECT * FROM productos WHERE idProd=$id";
			$consulta=$conexion->query($sql);
			$registro=$consulta->fetch_array();
		?>
		<form action="index.php?p=productos.php&accion=modificar&numeroDePagina=<?php echo $numeroDePagina?>" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label for="nombreProd">Nombre del producto:</label>
				<input type="text" class="form-control" name="nombreProd" id="nombreProd" value="<?php echo $registro['nombreProd']; ?>">
			</div> 

			<div class="form-group">
				<label for="descripcionProd">Descripción del producto:</label>
				<textarea class="form-control"  name="descripcionProd" id="descripcionProd"><?php echo $registro['descripcionProd']; ?></textarea>		
			</div>

			<div class="form-group">
				<label for="precioProd">Precio del producto:</label>
				<input class="form-control"  name="precioProd" id="precioProd" value="<?php echo $registro['precioProd']; ?>">
			</div>

			<div class="form-group">
				<label for="unidadesProd">Existencias del producto:</label>
				<input class="form-control"  name="unidadesProd" id="unidadesProd" value="<?php echo $registro['unidadesProd']; ?>">
			</div>

			<div class="form-group">
				<label for="fechaAlta">Fecha de alta del producto:</label>
				<input type="date" class="form-control"  name="fechaAlta" id="fechaAlta" value="<?php echo $registro['fechaAlta']; ?>">
			</div>

			<?php 
			if($registro['activado']==1){
				$checked='checked';
			}else{
				$checked='';
			}
			?>

			<div class="form-group">
				<label for="activado">Producto activado:</label>
				<input type="checkbox"  name="activado" id="activado" <?php echo $checked; ?>>
			</div>
			 
			<div class="form-group">
				<label for="fechaAlta">Imagenes del producto:</label><br>
				<?php
				//////////////////////////////////////////////////////////////////////////////
				//////// MODIFICACIÓN DE IMAGENES ////////////////////////////////////////////
				//////////////////////////////////////////////////////////////////////////////
				
				if(isset($_GET['idImagenBorrar'])){
					$sqli="DELETE FROM imagenes WHERE idImg=".$_GET['idImagenBorrar'];
					$consultai=$conexion->query($sqli);
				}
				//Accedo a la bbdd para mostrar las imagenes que elijo de la carpeta imagenes
			    $sqli="SELECT * FROM imagenes WHERE idProd=$id";
			    $consultai=$conexion->query($sqli);
			    while($registroi=$consultai->fetch_array()){
			    	?>
			    	<div style="display: inline-block; padding:5px; text-align: center;">
						<img src="imagenes/<?php echo $registroi['archivoImg'] ?>" width="235"><br>
						<a href="index.php?p=productos.php&accion=modificar&id=<?php echo $id; ?>&numeroDePagina=<?php echo $numeroDePagina; ?>&idImagenBorrar=<?php echo $registroi['idImg']; ?>" title="Borrar">
							<span style="color: red;" class="glyphicon glyphicon-trash"></span>
						</a>
			    	</div>
			    	<?php
			    } // Fin while mostrar imaganes
			    ?>
				
				<div class="form-group" id="imagenes">
			    	<label for="imagenes">
						Selecciona imagen a añadir:
						<a href="#">Agregar otra</a>
					</label>
					<input type="file" name="imagenes[]">
			    </div>

				<?php
			    //////////////////////////////////////////////////////////////////////////////
				//////// FIN DE MODIFICACIÓN DE IMAGENES ////////////////////////////////////////////
				//////////////////////////////////////////////////////////////////////////////
			     ?>
			</div>
			
			<input type="hidden" name="idProd" value="<?php echo $id; ?>">

			<button type="sumbit" name="enviar" class="btn btn-primary btn-lg btn-block">
				Enviar
			</button>
		</form>

		<?php 
		}
		?>

		<?php
		}else{
			?>
				<div class="alert alert-danger">No tienes permiso para realizar esta acción</div>;
			<?php
		} // Fin del if ($_SESSION['conectado'])
		break;

	//////////////////////////////////////////////////////////////////////
	/////////  FILTRADO DE PRODUCTOS POR SU FECHA DE ALTA  ///////////////
	//////////////////////////////////////////////////////////////////////
	case 'filtrado':
	$fechasql=$_GET['fechasql'];
	?>
	<h3>
		Productos filtrados introducidos el dia: 
		<small>
			<?php echo dimeFecha($fechasql); ?>	
		</small>
	</h3> 

	<?php

		if($_SESSION['conectado']){
		?>
		<h4>
			<a href="index.php?p=productos.php&accion=listado">
				Ver todos
			</a>
		</h4>
		<?php 
		}
		
		//Establecer la consulta a la base de datos en SQL
		
		$sql="SELECT * FROM productos WHERE fechaAlta='$fechasql'";
		//Ejecutar la pregunta o consulta
		$consulta=$conexion->query($sql);

		//Para hacer un listado con paginación. Primero averiguamos el numero total de resgistros
		$numeroTotalDeRegistros=$consulta->num_rows;
		//Establecer el numero de registros por pagina
		$numeroDeRegistrosPorPagina=4; //Lo ponemos a mano
		//Recogemos en que pagina estamos para mostrar los resultados que le corresponda
		if(isset($_GET['numeroDePagina'])){
			$numeroDePagina=$_GET['numeroDePagina'];
		}else{
			$numeroDePagina=0;
		}

		//Consulta según el numero de paginas obtenidas
		$inicioLimite=$numeroDePagina*$numeroDeRegistrosPorPagina;
		$sql="SELECT * FROM productos WHERE fechaAlta='$fechasql' LIMIT $inicioLimite,$numeroDeRegistrosPorPagina";

		//Vuelvo a ejecutar la consulta pero con su LIMIT ya paginado
		$consulta=$conexion->query($sql);

		//Procesamos los resultados de la pregunta con un bucle
		while($registro=$consulta->fetch_array()){
			?>
			<article>
				<header>
					<h4>
						<a href="index.php?p=productos.php&accion=ver&id=<?php echo $registro['idProd'];?>">
							<strong><?php echo $registro['nombreProd']; ?></strong>
						</a>

						<!-- <small>(<?php echo dimeFechaCorta($registro['fechaAlta']); ?>)</small> -->

						<?php 
						if($_SESSION['conectado']){
						?>
						-
						<a href="index.php?p=productos.php&accion=borrar&id=<?php echo $registro['idProd'];?>">
							<span class="glyphicon glyphicon-trash" style="color: red;"></span>
						</a>
						-
						<a href="index.php?p=productos.php&accion=modificar&id=<?php echo $registro['idProd'];?>">
							<span class="glyphicon glyphicon-pencil" style="color: green;"></span>
						</a>
						<?php } ?> 
					</h4>
					<!-- <small>
						<?php echo $registro['nombreCat']; ?>
					</small> -->
				</header>
			</article>
			<hr>
		<?php
		}

		?>	
		<div class="text-center">
			<ul class="pagination">
			<?php
				//Inicio if condicional para no seguir recorriendo paginas hacia atras cuando se llega a la pagina 0
			 if($numeroDePagina==0){ ?>
				<li class="disabled"><a href="#">&laquo;</a></li>
			<?php }else{?>
				<li>
			  		<a href="index.php?p=productos.php&accion=filtrado&numeroDePagina=<?php echo ($numeroDePagina-1); ?>&fechasql=<?php echo $fechasql ?>">&laquo;</a>
			  	</li>
			<?php
				} //Fin del if para no bajar de la pagina 0

				//Calculo el numero total de paginas
				$numeroTotalDePaginas=ceil($numeroTotalDeRegistros/$numeroDeRegistrosPorPagina);
					//Bucle para los numeros de las paginas que muestro
					for($numPagina=0;$numPagina<$numeroTotalDePaginas;$numPagina++){
						//Condicional para marcar la pagina activa y que la marque
						if($numeroDePagina==$numPagina){
							$activa='active';
						}else{
							$activa='';
						}
				?>

				<li class="<?php echo $activa; ?>">
					<a href="index.php?p=productos.php&accion=filtrado&numeroDePagina=<?php echo $numPagina; ?>&fechasql=<?php echo $fechasql ?>"><?php echo ($numPagina+1); ?>		
					</a>
				</li>

				<?php 
				} //Fin del bulce for
				?>
			
			<?php
				//Inicio if condicional para no sobrepasar recorriendo paginas hacia adeanate cuando se llega a la úlltima
			 if($numeroDePagina==($numeroTotalDePaginas-1)){ ?>
				<li class="disabled"><a href="#">&raquo;</a></li>
			<?php }else{?>
				<li>
			  		<a href="index.php?p=productos.php&accion=listado&numeroDePagina=<?php echo ($numeroDePagina+1); ?>&fechasql=<?php echo $fechasql ?>">&raquo;</a>
			  	</li>
			<?php
				} //Fin del if para no pasar de la última pagina 
			?>
			</ul>
		</div>
		<?php
		break;

	//////////////////////////////////////////////////////////////////////
	///////////////////////////  BUSCADOR  ///////////////////////////////
	//////////////////////////////////////////////////////////////////////
	case 'buscador':

		//Recojo la palabra de busqueda
		$palabra=$_GET['palabra'];
		?>
	<h4>
		Productos encontrados para: <?php echo $palabra; ?>	
	</h4> 
	<h4>
		<a href="index.php?p=productos.php&accion=listado">
			Ver todos los productos
		</a>
	</h4>
	<?php

		//Establezco la consulta en mi base de datos
		$sql="SELECT * FROM productos WHERE nombreProd LIKE '%$palabra%' OR descripcionProd LIKE '%$palabra%'";

		//Ejecuto la consulta
		$consulta=$conexion->query($sql);

		while ($registro=$consulta->fetch_array()){
			?>
			<article>
				<header class="buscador">
					<h4>
						<a href="index.php?p=productos.php&accion=ver&id=<?php echo $registro['idProd'];?>">
							<p class="enlinea"><?php echo str_ireplace($palabra, '<span class="label label-info"><strong><u>'.$palabra.'</u></strong></span>', $registro['nombreProd']); ?></p>
							-
							<p class="enlinea"><?php echo str_ireplace($palabra, '<span class="label label-info"><strong><u>'.$palabra.'</u></strong></span>', $registro['descripcionProd']); ?></p>
						</a>

						<!-- <small>(<?php echo dimeFechaCorta($registro['fechaAlta']); ?>)</small> -->

						<?php 
						if($_SESSION['conectado']){
						?>
						-
						<a href="index.php?p=productos.php&accion=borrar&id=<?php echo $registro['idProd'];?>">
							<span class="glyphicon glyphicon-trash" style="color: red;"></span>
						</a>
						-
						<a href="index.php?p=productos.php&accion=modificar&id=<?php echo $registro['idProd'];?>">
							<span class="glyphicon glyphicon-pencil" style="color: green;"></span>
						</a>
						<?php } ?> 
					</h4>
					<!-- <small>
						<?php echo $registro['nombreCat']; ?>
					</small> -->
				</header>
			</article>
			<?php
		} //Fin del while


		break;

}	//FIN DEL SWITCH($accion)
?>


