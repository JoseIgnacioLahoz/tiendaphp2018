<h2>
	Proveedores
	<?php 
	if($_SESSION['conectado']){
	?>
	-
	<small>
		<a href="index.php?p=proveedores.php&accion=insertar">Insertar Proveedor</a>
	</small>
	<?php } ?>
</h2>

<?php 
// Este archivo va a recibir una acción sino es así, listará los proveedores
if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}else{
	$accion='listado';
}

//Dependiendo de $accion la web hace una cosa u otra
switch ($accion) {
	/////////////////////////////////////////////////////////////
	/////////////// LISTADO DE PROVEEDORES  /////////////////////
	/////////////////////////////////////////////////////////////
	case 'listado':
		if($_SESSION['conectado']){
		?>
		<h4>
			<a href="index.php?p=proveedores.php&accion=insertar">
				Insertar proveedor
			</a>
		</h4>
		<?php 
		}

		//Establecer la consulta a la base de datos en SQL
			$sql="SELECT * FROM proveedores";

		//Ejecutar la pregunta o consulta
		$consulta=$conexion->query($sql);

		//Procesamos los resultados de la pregunta
		while($registro=$consulta->fetch_array()){
			?>
			<article>
				<header>
					<h4>
						<a href="index.php?p=proveedores.php&accion=ver&id=<?php echo $registro['idProveedor'];?>">
							<strong><?php echo $registro['nombreProveedor']; ?> </strong>
						</a>
						<?php 
						if($_SESSION['conectado']){
						?>
						-
						<a href="index.php?p=proveedores.php&accion=borrar&id=<?php echo $registro['idProveedor'];?>">
							<span class="glyphicon glyphicon-trash" style="color: red;"></span>
						</a>
						-
						<a href="index.php?p=proveedores.php&accion=modificar&id=<?php echo $registro['idProveedor'];?>">
							<span class="glyphicon glyphicon-pencil" style="color: green;"></span>
						</a>
						<?php } ?>
					</h4>
					<!-- <small>
						<?php echo $registro['nombreCat']; ?>
					</small> -->
				</header>
			</article>
		<?php
		}
		break;
	
	//////////////////////////////////////////////////////////////
	//////////////////    INSERTAR PROVEEDOR   /////////////////////
	//////////////////////////////////////////////////////////////
	case 'insertar':
		if($_SESSION['conectado']){
		?>
		<h3>
			Insertar un Proveedor
			-
			<small>
				<a href="index.php?p=proveedores.php">Volver / Cancelar</a>
			</small>
		</h3>
		<hr>

		<?php  
		if (isset($_POST['enviar'])){
			//Inserto el proveedor
			 
			//Recojo los datos que quiero insertar
			$nombre=$_POST['nombreProveedor'];
			$telefono=$_POST['telefonoProveedor'];

			//Muevo la imagen a la carpeta
			// move_uploaded_file($_FILES['imagen']['tmp_name'], 'imagenes/'.$nombreImagen);

			//Establezco la consulta
			$sql="INSERT INTO proveedores(nombreProveedor, telefonoProveedor)VALUES('$nombre', '$telefono')";

			//Ejecuto la consulta y/o Muestro el mensaje
			if($consulta=$conexion->query($sql)){
				header('Refresh: 2; url=index.php?p=proveedores.php');
				?>
				<div class="alert alert-success">
					<strong>TODO OK!!</strong>
					Insercion realizada con éxito
					<img src="imagenes/cargando.gif" width="50">
				</div>
				<?php	
			}else{
				?>
				<div class="alert alert-danger">
					<strong>ERROR!!</strong>
					No se ha podido realizar
				</div>
				<?php
			}
		}else{
			//Muestro el formulario de insercción
		?>
		<form action="index.php?p=proveedores.php&accion=insertar" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label for="nombre">Nombre del Proveedor:</label>
				<input type="text" class="form-control" name="nombreProveedor" id="nombreProveedor">
			</div> 

			<div class="form-group">
				<label for="correo">Teléfono del Proveedor:</label>
				<input type="text" class="form-control"  name="telefonoProveedor" id="telefonoProveedor">		
			</div>

			<button type="sumbit" name="enviar" class="btn btn-dfault">
				Enviar
			</button>
		</form>

		<?php
		} 
		}else{
			?>
				<div class="alert alert-danger">No tienes permiso para realizar esta acción</div>;
			<?php
		}// Fin del if ($_SESSION['conectado'])
		break;

	//////////////////////////////////////////////////////////////
	////////////////// VER DETALLE DEL PROVEEDOR   ///////////////
	//////////////////////////////////////////////////////////////
	case 'ver':
		?>	
		<h3>
			Detalle del Proveedor
		</h3>

	<?php 
	// Recojo el idProveedor que quiero mostrar
	$id=$_GET['id'];

	//Establezco una consulta segun su id de producto
	$sql="SELECT * FROM proveedores WHERE idProveedor=$id";

	// Ejecuto la consulta
	$consulta=$conexion->query($sql);

	// Extraigo los datos de dicha consulta
	$registro=$consulta->fetch_array();
	?>

	<article>
		<header>
			<h4>
				<strong><?php echo $registro['idProveedor']; ?></strong>
				<a href="index.php?p=proveedores.php"> - Volver</a>
			</h4>
		</header>
		<section>
			<div class="panel panel-success">
			  <div class="panel-heading">
			    <h3 class="panel-title">Nombre:</h3>
			  </div>
			  <div class="panel-body">
			    <?php echo $registro['nombreProveedor']; ?>
			  </div>
			</div>

			<div class="panel panel-info">
			  <div class="panel-heading">
			    <h3 class="panel-title">Teléfono:</h3>
			  </div>
			  <div class="panel-body">
			    <?php echo $registro['telefonoProveedor']; ?> 
			  </div>
			</div>
		</section>
	</article>
	<?php
		break;

	///////////////////////////////////////////////////////////////////
	////////////////  BORRAR UN PROVEEDOR DEL LISTADO  ////////////////
	//////////////////////////////////////////////////////////////////	

	case 'borrar':
	if($_SESSION['conectado']){
				//Cojo el id del PROVEEDOR a borrar
		$id=$_GET['id'];

		//Establezco la consulta
		$sql="DELETE FROM proveedores WHERE idProveedor=$id";

		//Ejecuto la consulta y/o Muestro mensaje
		if($consulta=$conexion->query($sql)){
			header('Refresh: 2; url=index.php?p=proveedores.php');
			?>
			<div class="alert alert-success">
				<strong>TODO OK!!</strong>
				Borrado realizado con exito
				<img src="imagenes/cargando.gif" width="50">
			</div>
			<?php		
		}else{
			?>
			<div class="alert alert-danger">
				<strong>ERROR!!</strong>
				No se ha podido realizar
			</div>
		<?php
		} 
		 }else{
			?>
				<div class="alert alert-danger">No tienes permiso para realizar esta acción</div>;
			<?php
		} // Fin del if ($_SESSION['conectado'])
		break;
		//////////////////////////////////////////////////////////////////////
		///////////////    MODIFICAR UN PROVEEDOR   //////////////////////////
		//////////////////////////////////////////////////////////////////////
		case 'modificar':
			if($_SESSION['conectado']){
		?>
		<h3>
			Modificar un Proveedor
			-
			<small>
				<a href="index.php?p=proveedores.php">Volver / Cancelar</a>
			</small>
		</h3>
		<hr>
		<?php
		if (isset($_POST['enviar'])){
			//Modifico el proveedor
			//Recojo los datos que quiero modificar
			$nombre=$_POST['nombreProveedor'];
			$telefono=$_POST['telefonoProveedor'];
			$id=$_POST['idProveedor'];
			
				//Establezco la consulta
			$sql="UPDATE proveedores SET nombreProveedor='$nombre', telefonoProveedor'$telefono' WHERE idProveedor=$id";

			//Ejecuto la consulta y/o Muestro el mensaje
			if($consulta=$conexion->query($sql)){
				header('Refresh: 2; url=index.php?p=proveedores.php');
				?>
				<div class="alert alert-success">
					<strong>TODO OK!!</strong>
					Modificación realizada con éxito
					<img src="imagenes/cargando.gif" width="50">
				</div>
				<?php	
			}else{
				?>
				<div class="alert alert-danger">
					<strong>ERROR!!</strong>
					No se ha podido realizar
				</div>
				<?php
			}
		}else{
			//Muestro el formulario de modificación
			//Necesito el id del proveedor  a modificar para poder actuar sobre él
			$id=$_GET['id'];
			$sql="SELECT * FROM proveedores WHERE idProveedor=$id";
			$consulta=$conexion->query($sql);
			$registro=$consulta->fetch_array();
		?>
		<form action="index.php?p=proveedores.php&accion=modificar" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label for="nombreProveedor">Nombre del Proveedor:</label>
				<input type="text" class="form-control" name="nombreProveedor" id="nombreProveedor" value="<?php echo $registro['nombreProveedor']; ?>">
			</div> 

			<div class="form-group">
				<label for="telefonoProveedor">Teléfono del Proveedor:</label>
				<input class="form-control"  name="telefonoProveedor" id="telefonoProveedor" value="<?php echo $registro['telefonoProveedor']; ?>">
			</div>

			<input type="hidden" name="idProveedor" value="<?php echo $id; ?>">

			<button type="sumbit" name="enviar" class="btn btn-dfault">
				Enviar
			</button>
		</form>

		<?php 
		}
		?>

		<?php
		}else{
			?>
				<div class="alert alert-danger">No tienes permiso para realizar esta acción</div>;
			<?php
		} // Fin del if ($_SESSION['conectado'])
			break;

}	//FIN DEL SWITCH($accion)
?>


