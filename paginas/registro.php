<!-- //////////////////////////////////////////////////////////////
//////////////////    REGISTRO DE NUEVO USUARIO   ////////////
////////////////////////////////////////////////////////////// -->
<h3>
	Registrar un nuevo Usuario
	-
	<small>
		<a href="index.php?p=productos.php">Volver / Cancelar</a>
	</small>
</h3>
<hr>

<?php  
if (isset($_POST['registro'])){
	$todoOK=true;
	$msgError='';
	//Inserto el usuario
			 
	//Recojo los datos que quiero insertar comprobando que se han rellenado de forma correcta
	$nombre=$_POST['nombre'];
	if(strLen($nombre)<3){
		$todoOK=false;
		$msgError.='El nombre debe tener al menos tres carácteres. ';
	}

	$apellidos=$_POST['apellidos'];
	if(strLen($apellidos)<3){
		$todoOK=false;
		$msgError.='Los apellidos deben tener al menos tres carácteres. ';
	}

	$login=$_POST['login'];
	if(strLen($login)<3){
		$todoOK=false;
		$msgError.='El login debe tener al menos tres carácteres. ';
	}

	$password=md5($_POST['password']);
	if(strLen($password)<4){
		$todoOK=false;
		$msgError.='El password debe tener al menos 4 carácteres. ';
	}

	$correo=$_POST['correo'];
	if(strLen($correo)<3){
		$todoOK=false;
		$msgError.='El email debe ser correcto.';
	}

	//Si todo rellenado de forma correcta:
	if($todoOK==true){

	//Muevo la imagen a la carpeta
	// move_uploaded_file($_FILES['imagen']['tmp_name'], 'imagenes/'.$nombreImagen);
	
		//Establezco la consulta
		$sql="INSERT INTO usuarios(nombre, apellidos, login, password, correo)VALUES('$nombre', '$apellidos', '$login', '$password', '$correo')";
		//Ejecuto la consulta y/o Muestro el mensaje
		if($consulta=$conexion->query($sql)){
			header('Refresh: 2; url=index.php?p=productos.php');
			?>
			<div class="alert alert-success">
				<strong>TODO OK!!</strong>
				Insercion realizada con éxito
				<img src="imagenes/cargando.gif" width="50">
			</div>
			<?php	
		}else{
			//Mensaje de error si no se ejectua bien la consulta sql
			?>
			<div class="alert alert-danger">
				<strong>ERROR!!</strong>
				No se ha podido realizar
			</div>
			<?php
		}

	}else{
		//Mensaje de error si no se rellenan bien los campos del formulario de registro
		?>
			<div class="alert alert-danger">
				<strong>ERROR!!</strong>
				<?php echo $msgError; ?>
			</div>
			<?php
	} //Fin del if($todoOK==true)

}else{
	//Muestro el formulario de registro
?>
<form action="index.php?p=registro.php" method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label for="nombre">Introduce tu Nombre:</label>
		<input type="text" class="form-control" name="nombre" id="nombre" required>
	</div> 

	<div class="form-group">
		<label for="apellidos">Introduce tus Apellidos:</label>
		<input class="form-control"  name="apellidos" id="apellidos" required>		
	</div>

	<div class="form-group">
		<label for="login">Introduce tu Login de Usuario:</label>
		<input type="text" class="form-control"  name="login" id="login" required>		
	</div>
	
	<div class="form-group">
		<label for="password">Introduce tu passoword:</label>
		<input type="password" class="form-control"  name="password" id="password" required>		
	</div>

	<div class="form-group">
		<label for="correo">Introduce tu Email:</label>
		<input type="email" class="form-control"  name="correo" id="correo" required>		
	</div>

	<button type="sumbit" name="registro" class="btn btn-primary btn-lg btn-block">
		Registro
	</button>
</form>
<?php } ?>