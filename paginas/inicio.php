<h3>Estás en Inicio</h3>



<section class="row">
<?php
//Establecer la consulta a la base de datos en SQL
if(isset($_GET['idCategoria'])){
	$idCategoria=$_GET['idCategoria'];
	$sql="SELECT * FROM noticias INNER JOIN categorias ON noticias.idCategoria=categorias.idCategoria WHERE noticias.idCategoria=$idCategoria";
}else{
	$sql="SELECT * FROM noticias INNER JOIN categorias ON noticias.idCategoria=categorias.idCategoria";
}

//Ejecutar la pregunta o consulta

$consulta=$conexion->query($sql);
$contador=1;
//Procesamos los resultados de la pregunta

while($registro=$consulta->fetch_array()){
	?>
	<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
		<header>
			<h4><strong><?php echo $registro['tituloNoticia']; ?></strong></h4>
		</header>
		<section>
			<img src="imagenes/<?php echo $registro['imagenNoticia']; ?>" class="img-responsive img-rounded" style="float:left; margin:10px; width: 200px;">
			<a href="index.php?p=detalle.php&idNoticia=<?php echo $registro['idNoticia'];?>">Leer más ...</a>
		</section>
	</div>
	<?php
	if($contador%4==0){
		echo '<div class="clearfix visible-lg-block"></div>';
	}
	if($contador%3==0){
		echo '<div class="clearfix visible-md-block"></div>';
	}
	if($contador%2==0){
		echo '<div class="clearfix visible-sm-block"></div>';
	}
	$contador++;
}
?>
</section>