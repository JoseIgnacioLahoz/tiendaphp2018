
<h3>
	Listado de Categorías
	<?php 
	if($_SESSION['conectado']){
	?>
	-
	<small>
		<a href="index.php?p=categorias.php&accion=insertar">Insertar categoría</a>
	</small>
	<?php } ?>
</h3>



<?php 
// Este archivo va a recibir una acción sino es así, listará los productos
if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}else{
	$accion='listado';
}

//Dependiendo de $accion la web hace una cosa u otra
switch ($accion) {
	/////////////////////////////////////////////////////////////
	/////////////// LISTADO DE CATEGORIAS  //////////////////////
	/////////////////////////////////////////////////////////////
	case 'listado':
	if($_SESSION['conectado']){
		?>
		<h4>
			<a href="index.php?p=categorias.php&accion=insertar">
				Insertar Categoría
			</a>
		</h4>
		<?php 
		}?>

		<table class="table table-hover table-striped">

			<?php 
			//LISTADO DE CATEGORIAS

			//Establezco una consulta segun su id de categoria
			$sql="SELECT * FROM categorias ORDER BY nombreCat ASC";

			// Ejecuto la consulta
			$consulta=$conexion->query($sql);

			// Extraigo y proceso los datos de dicha consulta y los muestro

			while($registro=$consulta->fetch_array()){
			?>
				<tr>
					<td>
						<?php echo $registro['nombreCat']; ?>
					</td>
					<?php 
						if($_SESSION['conectado']){
					?>
					<td class="text-right">
						<a href="index.php?p=categorias.php&accion=modificar&idCat=<?php echo $registro['idCat'];?>">Modificar</a>
						-
						<a href="index.php?p=categorias.php&accion=borrar&idCat=<?php echo $registro['idCat'];?>">Borrar</a>
					</td>
					<?php } ?>	
				</tr>
				<?php
			}
			?>
		</table>
		<hr>
	<?php
	
	break;

	//////////////////////////////////////////////////////////////
	////////////////// INSERTAR CATEGORÍA    /////////////////////
	//////////////////////////////////////////////////////////////
	case 'insertar':
		if($_SESSION['conectado']){
		?>
		<h3>
			Insertar nueva Categoría
			-
			<small>
				<a href="index.php?p=categorias.php">Volver / Cancelar</a>
			</small>
		</h3>
		<hr>

		<?php
		// INSERCION DE LA CATEGORIA EN LA BBDD 
		if(isset($_POST['enviar'])){
			$nombre=$_POST['nombre'];
			$descripcion=$_POST['descripcion'];
			$sql="INSERT INTO categorias(nombreCat, descripcionCat)VALUES('$nombre', '$descripcion')";

			//Ejecuto la consulta. 
			if($consulta=$conexion->query($sql)){
				header('Refresh: 2; url=index.php?p=categorias.php')
				?>
					<div class="alert alert-success">
						<strong>TODO OK!!</strong>
						Insercción realizada con éxito
						<img src="imagenes/cargando.gif" width="50">
					</div>
				<?php
			}else{
				?>
				<div class="alert alert-danger">
					<strong>ERROR!!</strong>
					No se ha podido realizar
				</div>
			<?php
			}
		}else{
			//Muestro el formulario de insercción
		?>

		<form action="index.php?p=categorias.php&accion=insertar" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label for="nombre">Nombre de la categoría:</label>
				<input type="text" class="form-control" name="nombre" id="nombre">
			</div> 

			<div class="form-group">
				<label for="nombre">Descripción de la categoría:</label>
				<input type="text" class="form-control" name="descripcion" id="descripcion">
			</div> 

			<button type="sumbit" name="enviar" class="btn btn-dfault">
				Insertar Categoría
			</button>
		</form>
		<?php
		} 
		}else{
			?>
				<div class="alert alert-danger">No tienes permiso para realizar esta acción</div>;
			<?php
		}// Fin del if ($_SESSION['conectado'])
		break;

	///////////////////////////////////////////////////////////////////
	////////////////  BORRAR UNA CATEGORÍA DEL LISTADO  //////////////
	//////////////////////////////////////////////////////////////////
	case 'borrar':
		if($_SESSION['conectado']){
		//Cojo el id de la categoria a borrar
		$idCat=$_GET['idCat'];

		//Establezco la consulta
		$sql="DELETE FROM categorias WHERE idCat=$idCat";

		//Ejecuto la consulta y/o Muestro mensaje

		if($consulta=$conexion->query($sql)){
			header('Refresh: 2; url=index.php?p=categorias.php');

			?>
			<div class="alert alert-success">
				<strong>TODO OK!!</strong>
				Borrado realizado con exito
				<img src="imagenes/cargando.gif" width="50">
			</div>
			<?php
			
		}else{
			?>
			<div class="alert alert-danger">
				<strong>ERROR!!</strong>
				No se ha podido realizar
			</div>
			<?php
		}
		}else{
			?>
				<div class="alert alert-danger">No tienes permiso para realizar esta acción</div>;
			<?php
		}// Fin del if ($_SESSION['conectado'])
		break;

	//////////////////////////////////////////////////////////////////////
	///////////////    MODIFICAR UNA CATEGORÍA   /////////////////////////
	//////////////////////////////////////////////////////////////////////	
	case 'modificar':
		if($_SESSION['conectado']){
		?>
		<h3>
			Modificar Categoría
			-
			<small>
				<a href="index.php?p=categorias.php">Volver / Cancelar</a>
			</small>
		</h3>
		<hr>

		<?php  
		if (isset($_POST['enviar'])){
			//Modifico la categoria	 
			//Recojo los datos que quiero modificar
			$nombre=$_POST['nombre'];
			$descripcionCat=$_POST['descripcionCat'];
			$id=$_POST['idCat'];

			//Establezco consulta
			$sql="UPDATE categorias SET nombreCat='$nombre', descripcionCat='$descripcionCat' WHERE idCat=$id";
			
			
			//Ejecuto la consulta y/o Muestro el mensaje
			if($consulta=$conexion->query($sql)){
				header('Refresh: 2; url=index.php?p=categorias.php');
				?>
				<div class="alert alert-success">
					<strong>TODO OK!!</strong>
					Modificación realizada con éxito
					<img src="imagenes/cargando.gif" width="50">
				</div>
				<?php	
			}else{
				?>
				<div class="alert alert-danger">
					<strong>ERROR!!</strong>
					No se ha podido realizar
				</div>
				<?php
			}
		}else{
			//Muestro el formulario de modificación
			//Necesito el id de la categoria a modificar para poder actuar sobre ella
			$idCat=$_GET['idCat'];
			$sql="SELECT * FROM categorias WHERE idCat=$idCat";
			$consulta=$conexion->query($sql);
			$registro=$consulta->fetch_array();
		?>
		<form action="index.php?p=categorias.php&accion=modificar" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label for="nombre">Nombre de la categoría:</label>
				<input type="text" class="form-control" name="nombre" id="nombre" value="<?php echo $registro['nombreCat']; ?>">
			</div> 

			<div class="form-group">
				<label for="descripcionCat">Descripción de la categoría:</label>
				<input type="text" class="form-control" name="descripcionCat" id="descripcionCat" value="<?php echo $registro['descripcionCat']; ?>">
			</div> 

			<input type="hidden" name="idCat" value="<?php echo $idCat; ?>">

			<button type="sumbit" name="enviar" class="btn btn-dfault">
				Enviar
			</button>
		</form>

		<?php 
		}
		}else{
			?>
				<div class="alert alert-danger">No tienes permiso para realizar esta acción</div>;
			<?php
		}// Fin del if ($_SESSION['conectado'])
		break;

}	//FIN DEL SWITCH($accion)
?>