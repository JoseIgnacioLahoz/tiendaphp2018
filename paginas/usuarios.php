<h2>
	Usuarios
	<?php 
	if($_SESSION['conectado']){
	?>
	-
	<small>
		<a href="index.php?p=usuarios.php&accion=insertar">Insertar usuario</a>
	</small>
	<?php } ?>
</h2>

<?php 
// Este archivo va a recibir una acción sino es así, listará los productos
if(isset($_GET['accion'])){
	$accion=$_GET['accion'];
}else{
	$accion='listado';
}

//Dependiendo de $accion la web hace una cosa u otra
switch ($accion) {
	/////////////////////////////////////////////////////////////
	/////////////// LISTADO DE USUARIOS  ///////////////////////
	/////////////////////////////////////////////////////////////
	case 'listado':
		if($_SESSION['conectado']){
		?>
		<h4>
			<a href="index.php?p=usuarios.php&accion=insertar">
				Insertar usuario
			</a>
		</h4>
		<?php 
		

		//Establecer la consulta a la base de datos en SQL
			$sql="SELECT * FROM usuarios";

		//Ejecutar la pregunta o consulta
		$consulta=$conexion->query($sql);

		//Procesamos los resultados de la pregunta
		while($registro=$consulta->fetch_array()){
			?>
			<article>
				<header>
					<h4>
						<a href="index.php?p=usuarios.php&accion=ver&id=<?php echo $registro['idUsuario'];?>">
							<strong><?php echo $registro['nombre']; ?>, <?php echo $registro['apellidos']; ?></strong>
						</a>
						<?php 
						if($_SESSION['conectado']){
						?>
						-
						<a href="index.php?p=usuarios.php&accion=borrar&id=<?php echo $registro['idUsuario'];?>">
							<span class="glyphicon glyphicon-trash" style="color: red;" title="Borrar Usuario"></span>
						</a>
						-
						<a href="index.php?p=usuarios.php&accion=modificar&id=<?php echo $registro['idUsuario'];?>">
							<span class="glyphicon glyphicon-pencil" style="color: green;" title="Editar Usuario"></span>
						</a>
						<?php } ?>
					</h4>
					<!-- <small>
						<?php echo $registro['nombreCat']; ?>
					</small> -->
				</header>
			</article>
			<?php	
			}	
		}else{
			?>
				<div class="alert alert-danger">No tienes permiso para realizar esta acción</div>;
			<?php

		} // Fin del if ($_SESSION['conectado'])
		break;
	
	//////////////////////////////////////////////////////////////
	//////////////////    INSERTAR USUARIO   /////////////////////
	//////////////////////////////////////////////////////////////
	case 'insertar':
		if($_SESSION['conectado']){
		?>
		<h3>
			Insertar un Usuario
			-
			<small>
				<a href="index.php?p=usuarios.php">Volver / Cancelar</a>
			</small>
		</h3>
		<hr>

		<?php  
		if (isset($_POST['enviar'])){
			//Inserto el usuario
			 
			//Recojo los datos que quiero insertar
			$nombre=$_POST['nombre'];
			$apellidos=$_POST['apellidos'];
			$login=$_POST['login'];
			$correo=$_POST['correo'];

			//Muevo la imagen a la carpeta
			// move_uploaded_file($_FILES['imagen']['tmp_name'], 'imagenes/'.$nombreImagen);

			//Establezco la consulta
			$sql="INSERT INTO usuarios(nombre, apellidos, login, correo)VALUES('$nombre', '$apellidos', '$login', '$correo')";

			//Ejecuto la consulta y/o Muestro el mensaje
			if($consulta=$conexion->query($sql)){
				header('Refresh: 2; url=index.php?p=usuarios.php');
				?>
				<div class="alert alert-success">
					<strong>TODO OK!!</strong>
					Insercion realizada con éxito
					<img src="imagenes/cargando.gif" width="50">
				</div>
				<?php	
			}else{
				?>
				<div class="alert alert-danger">
					<strong>ERROR!!</strong>
					No se ha podido realizar
				</div>
				<?php
			}
		}else{
			//Muestro el formulario de insercción
		?>
		<form action="index.php?p=usuarios.php&accion=insertar" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label for="nombre">Nombre del Usuario:</label>
				<input type="text" class="form-control" name="nombre" id="nombre">
			</div> 

			<div class="form-group">
				<label for="apellidos">Apellidos del Usuario:</label>
				<textarea class="form-control"  name="apellidos" id="apellidos"></textarea>		
			</div>

			<div class="form-group">
				<label for="login">Login del Usuario:</label>
				<input type="text" class="form-control"  name="login" id="login">		
			</div>

			<div class="form-group">
				<label for="correo">Email del Usuario:</label>
				<input type="text" class="form-control"  name="correo" id="correo">		
			</div>

			<button type="sumbit" name="enviar" class="btn btn-primary btn-lg btn-block">
				Enviar
			</button>
		</form>

		<?php
		} 
		}else{
			?>
				<div class="alert alert-danger">No tienes permiso para realizar esta acción</div>;
			<?php
		}// Fin del if ($_SESSION['conectado'])
		break;

	//////////////////////////////////////////////////////////////
	////////////////// VER DETALLE DEL USUARIO   ////////////////
	//////////////////////////////////////////////////////////////
	case 'ver':
		?>	
		<h3>
			Detalle del Usuario
		</h3>

	<?php 
	// Recojo el idUsuario que quiero mostrar
	// $id=$_GET['id'];
	$id=$_SESSION['conectado']['idUsuario'];

	//Establezco una consulta segun su id de producto
	$sql="SELECT * FROM usuarios WHERE idUsuario=$id";

	// Ejecuto la consulta
	$consulta=$conexion->query($sql);

	// Extraigo los datos de dicha consulta
	$registro=$consulta->fetch_array();
	?>

	<article>
		<header>
			<h4>
				<strong><?php echo $registro['idUsuario']; ?></strong>
				<a href="index.php?p=usuarios.php"> - Volver</a>
			</h4>
		</header>
		<section>
			<div class="panel panel-success">
			  <div class="panel-heading">
			    <h3 class="panel-title">Nombre:</h3>
			  </div>
			  <div class="panel-body">
			    <?php echo $registro['nombre']; ?>
			  </div>
			</div>

			<div class="panel panel-info">
			  <div class="panel-heading">
			    <h3 class="panel-title">Apellidos:</h3>
			  </div>
			  <div class="panel-body">
			    <?php echo $registro['apellidos']; ?>
			  </div>
			</div>

			<div class="panel panel-info">
			  <div class="panel-heading">
			    <h3 class="panel-title">Login:</h3>
			  </div>
			  <div class="panel-body">
			    <?php echo $registro['login']; ?> 
			  </div>
			</div>

			<div class="panel panel-info">
			  <div class="panel-heading">
			    <h3 class="panel-title">Email:</h3>
			  </div>
			  <div class="panel-body">
			    <?php echo $registro['correo']; ?> 
			  </div>
			</div>
			
			<a href="index.php?p=usuarios.php&accion=modificar&id=<?php echo $_SESSION['conectado']['idUsuario']?>">
				Modificar datos de este usuario
			</a>
				
		</section>
	</article>
	<?php
		break;

	///////////////////////////////////////////////////////////////////
	////////////////  BORRAR UN USUARIO DEL LISTADO  ////////////////
	//////////////////////////////////////////////////////////////////	

	case 'borrar':
	if($_SESSION['conectado']){
				//Cojo el id del usuario a borrar
		$id=$_GET['id'];

		//Establezco la consulta
		$sql="DELETE FROM usuarios WHERE idUsuario=$id";

		//Ejecuto la consulta y/o Muestro mensaje
		if($consulta=$conexion->query($sql)){
			header('Refresh: 2; url=index.php?p=usuarios.php');
			?>
			<div class="alert alert-success">
				<strong>TODO OK!!</strong>
				Borrado realizado con exito
				<img src="imagenes/cargando.gif" width="50">
			</div>
			<?php		
		}else{
			?>
			<div class="alert alert-danger">
				<strong>ERROR!!</strong>
				No se ha podido realizar
			</div>
		<?php
		} 
		 }else{
			?>
				<div class="alert alert-danger">No tienes permiso para realizar esta acción</div>;
			<?php
		} // Fin del if ($_SESSION['conectado'])
		break;
		//////////////////////////////////////////////////////////////////////
		///////////////    MODIFICAR UN USUARIO   //////////////////////////
		//////////////////////////////////////////////////////////////////////
		case 'modificar':
			if($_SESSION['conectado']){
		?>
		<h3>
			Modificar un Usuario
			-
			<small>
				<a href="index.php?p=usuarios.php">Volver / Cancelar</a>
			</small>
		</h3>
		<hr>
		<?php
		if (isset($_POST['enviar'])){
			//Modifico el usuario
			//Recojo los datos que quiero modificar
			$nombre=$_POST['nombre'];
			$apellidos=$_POST['apellidos'];
			$login=$_POST['login'];
			$correo=$_POST['correo'];
			$id=$_SESSION['conectado']['idUsuario'];
			
				//Establezco la consulta
			$sql="UPDATE usuarios SET nombre='$nombre', apellidos='$apellidos', login='$login', correo='$correo' WHERE idUsuario='$id'";

			//Ejecuto la consulta y/o Muestro el mensaje
			if($consulta=$conexion->query($sql)){
				header('Refresh: 2; url=index.php?p=usuarios.php');
				?>
				<div class="alert alert-success">
					<strong>TODO OK!!</strong>
					Modificación realizada con éxito
					<img src="imagenes/cargando.gif" width="50">
				</div>
				<?php	
			}else{
				?>
				<div class="alert alert-danger">
					<strong>ERROR!!</strong>
					No se ha podido realizar
				</div>
				<?php
			}
		}else{
			//Muestro el formulario de modificación
			//Necesito el id del producto  a modificar para poder actuar sobre él
			$id=$_SESSION['conectado']['idUsuario'];
			$sql="SELECT * FROM usuarios WHERE idUsuario=$id";
			$consulta=$conexion->query($sql);
			$registro=$consulta->fetch_array();
		?>
		<form action="index.php?p=usuarios.php&accion=modificar" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label for="nombreProd">Nombre del Usuario:</label>
				<input type="text" class="form-control" name="nombre" id="nombre" value="<?php echo $registro['nombre']; ?>">
			</div> 

			<div class="form-group">
				<label for="apellidos">Apellidos del Usuario:</label>
				<textarea class="form-control"  name="apellidos" id="apellidos"><?php echo $registro['apellidos']; ?></textarea>		
			</div>

			<div class="form-group">
				<label for="login">Login del Usuario:</label>
				<input class="form-control"  name="login" id="login" value="<?php echo $registro['login']; ?>">
			</div>

			<div class="form-group">
				<label for="correo">Email del Usuario:</label>
				<input type="email" class="form-control"  name="correo" id="correo" value="<?php echo $registro['correo']; ?>">
			</div>

			<input type="hidden" name="idProd" value="<?php echo $id; ?>">

			<button type="sumbit" name="enviar" class="btn btn-primary btn-lg btn-block">
				Enviar
			</button>
		</form>

		<?php 
		}
		?>

		<?php
		}else{
			?>
				<div class="alert alert-danger">No tienes permiso para realizar esta acción</div>;
			<?php
		} // Fin del if ($_SESSION['conectado'])
			break;

}	//FIN DEL SWITCH($accion)
?>


