-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-05-2018 a las 13:53:52
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tienda`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `idCat` int(11) NOT NULL,
  `nombreCat` varchar(25) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `descripcionCat` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `imagenCat` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`idCat`, `nombreCat`, `descripcionCat`, `imagenCat`) VALUES
(1, 'Primavera', 'Los artículos mas vistosos y alegres para esta primavera', NULL),
(2, 'Verano', 'Para disfrutar de un fresquito y divertido verano playero', NULL),
(3, 'Otoño', 'Los colores más cálidos y serios para la transición del calor al frío', NULL),
(4, 'Invierno', 'Prepárate para no pasar frío y estar actualizado en tu indumentaria invernal', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes`
--

CREATE TABLE `imagenes` (
  `idImg` int(11) NOT NULL,
  `descripcionImg` text COLLATE utf8_spanish2_ci,
  `archivoImg` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `idProd` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `imagenes`
--

INSERT INTO `imagenes` (`idImg`, `descripcionImg`, `archivoImg`, `idProd`) VALUES
(1, NULL, '1526988985_boina1.jpg', 21),
(2, NULL, '1526991168_boina2.jpg', 21),
(3, NULL, '1526987700_boina3.jpg', 21),
(4, NULL, '1526989993_gafas1.jpg', 22),
(5, NULL, '1526988776_gafas2.jpg', 22),
(6, NULL, '1526990371_gafas3.jpg', 22),
(7, NULL, '1526988336_zapato1.jpg', 23),
(8, NULL, '1526996629_zapato2.jpg', 23),
(9, NULL, '1526995985_zapato3.jpg', 23),
(10, NULL, '1527076059_1526991168_boina2.jpg', 1),
(11, NULL, '1527102469_bufanda1.jpg', 2),
(12, NULL, '1527107773_bufanda2.jpg', 2),
(13, NULL, '1527105579_bufanda3.jpg', 2),
(14, NULL, '1527108382_boina4.jpg', 1),
(15, NULL, '1527099754_boina5.jpg', 1),
(16, NULL, '1527105975_guantes1.jpg', 3),
(17, NULL, '1527099081_guantes2.jpg', 3),
(18, NULL, '1527107048_guantes3.jpg', 3),
(19, NULL, '1527101978_orejeras1.jpg', 4),
(20, NULL, '1527106768_orejeras2.jpg', 4),
(21, NULL, '1527107765_orejeras3.jpg', 4),
(22, NULL, '1527104690_botas1.jpg', 5),
(23, NULL, '1527106829_botas2.jpg', 5),
(24, NULL, '1527106603_botas3.jpg', 5),
(25, NULL, '1527107340_anork1.jpg', 6),
(26, NULL, '1527103561_anork2.jpg', 6),
(27, NULL, '1527101004_anork3.jpg', 6),
(28, NULL, '1527106040_camisa1.jpg', 7),
(29, NULL, '1527101375_camisa2.jpg', 7),
(30, NULL, '1527107995_camisa3.jpg', 7),
(31, NULL, '1527106812_guantesb1.jpg', 8),
(32, NULL, '1527102033_guantesb2.jpg', 8),
(33, NULL, '1527105485_guantesb3.jpg', 8),
(34, NULL, '1527108275_gafasc1.jpg', 9),
(35, NULL, '1527108690_gafasc2.jpg', 9),
(36, NULL, '1527100669_gafasc3.jpg', 9),
(37, NULL, '1527109743_pantalonc1.jpg', 10),
(38, NULL, '1527103192_pantalonc2.jpg', 10),
(39, NULL, '1527109439_pantalonc3.jpg', 10),
(40, NULL, '1527108886_chaqueta1.jpg', 11),
(41, NULL, '1527106200_chaqueta2.jpg', 11),
(42, NULL, '1527105136_chaqueta3.jpg', 11),
(43, NULL, '1527103937_traje1.jpg', 12),
(44, NULL, '1527106614_traje2.jpg', 12),
(45, NULL, '1527101457_traje3.jpg', 12),
(46, NULL, '1527110538_sombrero1.jpg', 17),
(47, NULL, '1527105675_sombrero2.jpg', 17),
(48, NULL, '1527108465_sombrero3.jpg', 17),
(49, NULL, '1527107488_sandalias1.jpg', 15),
(50, NULL, '1527101800_sandalias2.jpg', 15),
(51, NULL, '1527109222_sandalias3.jpg', 15),
(52, NULL, '1527163512_zapa1.jpg', 14),
(53, NULL, '1527160278_zapa2.jpg', 14),
(54, NULL, '1527157314_zapa3.jpg', 14),
(55, NULL, '1527165931_moca1.jpg', 16),
(56, NULL, '1527166020_moca2.jpg', 16),
(57, NULL, '1527161235_moca3.jpg', 16);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pagos`
--

CREATE TABLE `pagos` (
  `idPago` int(11) NOT NULL,
  `nombrePago` varchar(25) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `logo` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `idPedido` int(11) NOT NULL,
  `fechaPedido` date NOT NULL,
  `cantidadPedido` int(11) NOT NULL,
  `idProd` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `idPago` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `idProd` int(11) NOT NULL,
  `nombreProd` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcionProd` text COLLATE utf8_spanish2_ci,
  `precioProd` double(5,2) NOT NULL,
  `unidadesProd` int(11) NOT NULL,
  `fechaAlta` date NOT NULL,
  `activado` bit(1) NOT NULL,
  `idCat` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`idProd`, `nombreProd`, `descripcionProd`, `precioProd`, `unidadesProd`, `fechaAlta`, `activado`, `idCat`) VALUES
(1, 'boina', 'boina muy bonita de lana', 25.00, 10, '2018-05-15', b'1', 0),
(2, 'bufanda', 'Bufanda de inverno lana pura a colorines', 15.00, 5, '2018-05-03', b'1', 0),
(3, 'guantes', 'Guantes de  piel muy abrigados para crudos inviernos', 50.00, 50, '2018-04-11', b'1', 0),
(4, 'orejeras', 'Producto de importación muy exitoso', 15.00, 150, '2018-05-08', b'1', 0),
(5, 'botas de agua', 'Para días lluviosos, boticas con forro de piel, muy calenticas e impermeables', 65.00, 25, '2018-05-03', b'1', 0),
(6, 'anorak', 'Doble capa con relleno de plumas', 125.00, 50, '2018-05-03', b'1', 0),
(7, 'camisa', ' termica muy calentica', 10.00, 100, '2018-05-08', b'1', 0),
(8, 'guantes bluetooth', 'con bluetooth todo es mejor', 25.00, 200, '2018-05-08', b'1', 0),
(9, 'gafas de sol', 'En todos los colores y para todo tipo de soles', 50.00, 25, '2018-05-15', b'1', 0),
(10, 'pantalón corto', 'Aventura y paseo multitarea y multitejido', 30.00, 100, '2018-05-15', b'1', 0),
(11, 'chaqueta americana', 'Producto de lino para un entretiempo entreverado', 125.00, 10, '2018-05-15', b'1', 0),
(12, 'traje azul', 'Bonito conjunto de americana y pantalón de hilo fino', 225.00, 15, '2018-05-15', b'1', 0),
(13, 'camisa de cuadros gris', 'Cuadros para todos los gustos de tamaños y formas distintas', 15.00, 50, '2018-05-17', b'0', 0),
(14, 'zapatillas paseo', 'Zapatillas de tela de toda la vida transpirables', 30.00, 25, '2018-05-17', b'1', 0),
(15, 'sandalias romanas', 'de badana fina y suela resistente de caucho', 50.00, 10, '2018-05-18', b'1', 0),
(16, 'mocasines chica', 'típicos de india de toda la vida', 15.00, 150, '2018-05-16', b'1', 0),
(17, 'sombrero de paja', 'Para todo el mundo proteje del sol y refresca el ambiente', 25.00, 25, '2018-05-09', b'1', 0),
(18, 'toalla playera a raya', 'Grande y muy suave', 100.00, 5, '2018-05-09', b'0', 0),
(19, 'chaqueta de punto rosa', 'Para chicas fresquitas fina y elegante', 40.00, 10, '2018-05-09', b'0', 0),
(20, 'bufanda a cuadros', 'Franela de alta calidad', 75.00, 10, '2018-05-15', b'1', 0),
(21, 'Otra boina', 'variedad de boinas de todas las formas', 50.00, 25, '2018-05-22', b'1', 0),
(22, 'Gafas de sol modernas', 'Gafas muy modernas y aptas para todos los públicos', 75.00, 50, '2018-05-22', b'1', 0),
(23, 'Zapato ', 'Gran variedad de zapatos para todos los usos', 125.00, 50, '2018-05-22', b'1', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `idProveedor` int(11) NOT NULL,
  `nombreProveedor` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `telefonoProveedor` varchar(9) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`idProveedor`, `nombreProveedor`, `telefonoProveedor`) VALUES
(1, 'José Proveedor Uno', '976000001'),
(2, 'Pepe Proveedor Dos', '976000002'),
(3, 'Pepe Luis Proveedor Tres', '976000003');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL,
  `nombre` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `apellidos` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `login` varchar(25) COLLATE utf8_spanish2_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `correo` varchar(25) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `session` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `avatar` varchar(100) COLLATE utf8_spanish2_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idUsuario`, `nombre`, `apellidos`, `login`, `password`, `correo`, `session`, `avatar`) VALUES
(1, 'Jose Ignacio', 'Lahoz Felez', 'admin', 'ee10c315eba2c75b403ea99136f5b48d', 'admin@gmail.com', 'c755552f5bb589632331d8efc35d5d32', NULL),
(2, 'Pepe Lui', 'Leches Frescas', 'Alparcero', 'ee10c315eba2c75b403ea99136f5b48d', 'pepe@gmail.com', '', NULL),
(3, 'Pepito', 'Listillo caprichoso', 'Listin', '81dc9bdb52d04dc20036dbd8313ed055', 'pepito@gmail.com', '', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`idCat`);

--
-- Indices de la tabla `imagenes`
--
ALTER TABLE `imagenes`
  ADD PRIMARY KEY (`idImg`);

--
-- Indices de la tabla `pagos`
--
ALTER TABLE `pagos`
  ADD PRIMARY KEY (`idPago`);

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`idPedido`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idProd`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`idProveedor`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idUsuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categorias`
--
ALTER TABLE `categorias`
  MODIFY `idCat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `imagenes`
--
ALTER TABLE `imagenes`
  MODIFY `idImg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT de la tabla `pagos`
--
ALTER TABLE `pagos`
  MODIFY `idPago` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `idPedido` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `idProd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `idProveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
