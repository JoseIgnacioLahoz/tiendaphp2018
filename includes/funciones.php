<?php 
//Declaro variables globales
$dias=['domingo', 'lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado'];
$meses=['','enero', 'febrero', 'marzo','abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'];


//Fichero con varias funciones para la web
//Funcion Fecha larga con el dia de la semana (Lunes, 1 de enero de 2018)
function dimeFecha($lafecha){
	global $meses, $dias;
	$fecha = new DateTime($lafecha);
	return $dias[$fecha->format('w')].', '.$fecha->format('d').' de '.$meses[$fecha->format('n')].' de '.$fecha->format('Y');
} //Fin de la función dimeFecha()
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Funcion fecha corta dia mes y año (1 - enero - 2018)
function dimeFechaCorta($lafecha){
	global $meses;
	$fecha = new DateTime($lafecha);
	return $fecha->format('j').' - '.$meses[$fecha->format('n')].' - '.$fecha->format('Y');
} //Fin de la función dimeFechaCorta()
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Función para devolver una tabla en html, con el mes que le pasemos
function dimeMes($mes, $anyo){
	global $meses;
	global $conexion;

	$r='';
	$r.='<table class="table table-striped"';
	$r.='<tr>';
	$r.='<th colspan="7" class="text-center">'.ucfirst($meses[$mes]).' - '.$anyo.'</tr>';
	$r.='</tr>';
	$r.='<tr>';
	$r.='<th class="text-center">L</th>';
	$r.='<th class="text-center">M</th>';
	$r.='<th class="text-center">X</th>';
	$r.='<th class="text-center">J</th>';
	$r.='<th class="text-center">V</th>';
	$r.='<th class="text-center">S</th>';
	$r.='<th class="text-center">D</th>';
	$r.='</tr>';

	//Creo la fecha timestamp del dia 1 del mes y año dado
	$f=mktime(0,0,0,$mes, 1, $anyo);
	$diaSemana=date('N',$f); //(Con 'N' numeramos de lunes 1 a domingo 7)

	//Relleno las celdas en blanco los dias vacios
	if($diaSemana>1){
		$r.='<tr>';
		for($i=1;$i<$diaSemana;$i++){
			$r.='<td></td>';
		}
	}
	//creo las variabels necesarias
	$fechahoy=mktime(0,0,0,date('n'),date('j'),date('Y'));
	$ultimoDiaDelMes=date('t',$f);
	for($dia=1;$dia<=$ultimoDiaDelMes;$dia++){
		$fecha=mktime(0,0,0,$mes,$dia,$anyo);
		//Si el el dia es lunes abro tr
		if(date('w',$fecha)==1){
			$r.='<tr>';
		}

		//Establezco las clase css que va a tener el dia
		$clasedia='text-center';
		
		
		//Si el dia es el de hoy, le agrego una clase
		if($fechahoy==$fecha){
			$clasedia.=' info';
		}

		//Si ese dia, tiene productos dados de alta, le pongo otra clase
		//Para esto necesito hacer una consulta a la base de datos
		$fechasql=date('Y-m-d', $fecha);
		$sql="SELECT * FROM productos WHERE fechaAlta='$fechasql'";

		//Ejecuto la consulta
		$consulta=$conexion->query($sql);

		//Creo variable con el contenido del dia
		$eldia=$dia;

		//Si hay registros aplico clases y agrego enlaces
		if($consulta->num_rows>0){
			$eldia='<a href="index.php?p=productos.php&accion=filtrado&fechasql='.$fechasql.'" class="label label-success">'.$dia.'</a>';
		}

		//Dibujo el dia
		$r.='<td class="'.$clasedia.'">'.$eldia.'</td>';

		//Si es domingo cierro tr
		if(date('w',$fecha)==0){
			$r.='</tr>';
		}
	} // Fin del blucle for (Principal)


	$r.='</table>';
	return $r;
} //Fin de la funcion dimeMes()



?>